package com.example.testthread;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestThreadApplicationTests {


    @Test
    void contextLoads() {

        TsThread thread1 = new TsThread();
        TsThread tsThread = new TsThread();
        thread1.run();
        tsThread.run();

    }

}
