package com.tl.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.web.bind.annotation.GetMapping;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket1(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("a");
    }
    @Bean
    public Docket docket2(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("b");
    }
    
    //配置了Swagger的Docket的bean实例
    @Bean
    public Docket docket(Environment environment){
        Profiles profiles=Profiles.of("dev");
        Boolean b=environment.acceptsProfiles(profiles);
        boolean flag=false;
        if(b){
            flag=true;
        }
        return new Docket(DocumentationType.SWAGGER_2).groupName("tul").apiInfo(apiInfo())
                .enable(flag) // 关闭swagger
                .select()
                //RequestHandlerSelectors,配置要扫描接口的方式
                //basePackage("com.tl.controller") 扫描包
                //any() 扫描全部
                //none()都不扫描
                //   withClassAnnotation()  扫描类上的注解，参数是一个注解的反射对象
                //withMethodAnnotation() 扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.tl.controller"))
                //.paths(PathSelectors.ant("/tl/**"))  过滤掉不扫描
                .build();
    }

    //配置Swagger信息=apiInfo 访问 http://localhost:8080/swagger-ui.html
    private ApiInfo apiInfo() {
        //作者信息
       Contact contact= new Contact("涂亮", "http://www.baidu.com", "405208660@qq.com");
       return new ApiInfo("文档管理api",
               "即使在小的船也能远航",
               "v1.0",
               "www.baidu.com",
               contact, "Apache 2.0",
               "http://www.apache.org/licenses/LICENSE-2.0",
               new ArrayList());
    }
}
