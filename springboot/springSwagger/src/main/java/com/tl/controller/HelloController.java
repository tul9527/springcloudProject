package com.tl.controller;


import com.tl.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {
    @ApiOperation("不带参数的测试")
    @GetMapping(value = "/hello")
    public String hello(){
        return "hello";
    }

    //只要我们的接口中，返回值中存在实体类，他就会被扫描到swagger中
     @PostMapping(value = "/user")
    public User user(){
        return  new User();
     }

     @ApiOperation("hello1你好类")
     @GetMapping(value = "/hello1")
     public String hello2(@ApiParam("用户名") String username){
        return "hello";
     }

     @ApiOperation("Post测试类")
     @PostMapping(value = "/postt")
     public  User postt(@ApiParam("用户名") User user){
         System.out.println("user:"+user);
        return user;
     }
}
