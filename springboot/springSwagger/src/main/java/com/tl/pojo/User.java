package com.tl.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel("用户实体类")
public class User {
    @ApiModelProperty("id")
    public String id;
    @ApiModelProperty("用户名")
    public  String username;
    @ApiModelProperty("密码")
    public  String pwd;
}
