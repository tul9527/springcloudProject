package com.tl.springsecurity.config;


import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


//AOP: 拦截器
@EnableWebSecurity
public class securityConfig extends WebSecurityConfigurerAdapter {
   //授权
    @Override
    protected void configure(HttpSecurity http) throws Exception {
     //首页所有人都可以访问，功能页只有权限的人才能访问,请求授权的规则。
          http.authorizeHttpRequests().antMatchers("/").permitAll()
                  .antMatchers("/level1/**").hasRole("vip1")
                  .antMatchers("/level2/**").hasRole("vip2")
                  .antMatchers("/level3/**").hasRole("vip3");
    //没有权限返回登录页面
        http.formLogin();
        //注销功能
        http.logout().logoutSuccessUrl("/");
    }
    //认证
    //在Spring Security 5.0+  新增了很多的加密方式 : passwordEncoder
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder()).withUser("tl123").password(new BCryptPasswordEncoder().encode("123456")).roles("vip1","vip2")
                .and().withUser("tl").password(new BCryptPasswordEncoder().encode("123123")).roles("vip1","vip3");
    }
}
