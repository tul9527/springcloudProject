package com.tl;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tl.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.Jedis;

import java.util.*;


@SpringBootTest
class TestRedisApplicationTests {

    @Autowired
    @Qualifier("customRedisTemplate")
    private RedisTemplate<String,Object> redisTemplate;
     @Test
    void contextLoads() {
        //redisTemplate 操作不同数据类型，api和我们的指令是一样的
        //opsForValue 操作字符串 类似String
        //opsForSet
        //除了基本的操作，我们常用的方法都可以直接通过redisTemplete操作，比如事务
        //获取redis的连接对象
        //RedisConnection connection=redisTemplate.getConnectionFactory.getConnection();
        redisTemplate.opsForValue().set("mykey","tuliang");
        System.out.println(redisTemplate.opsForValue().get("mykey"));
    }

    @Test
    void test() throws JsonProcessingException {
         //真实开发一般都使用json来传对象
        User user=new User("admin",3);
        User user1=new User("user",5);
        List list=new ArrayList();
        list.add(user);
        list.add(user1);
        //将对象变为json对象
        //jsonUser=new ObjectMapper().writeValueAsString(user);
        String jUser=  new ObjectMapper().writeValueAsString(list);
        //redisTemplate.opsForValue().set("aaa",jUser);
        //String json = redisTemplate.opsForValue().get("user").toString();
       String json=redisTemplate.opsForValue().get("t1").toString();
//       redisTemplate.multi();
//       redisTemplate.opsForValue().
       List list1=Arrays.asList(json) ;
       list1.forEach(System.out::println);
        System.out.println(json);
    }
    @Test
    void TestJedis(){
        Jedis jedis=new Jedis("127.0.0.1",6379);
         jedis.lpush("name","1","2","3","4","3");
        List<String> name = jedis.lrange("name", 0, -1);
        name.forEach(System.out::println);
    }
   //hash
    @Test
    void TestJedis1(){
         HashMap<String,String> map=new HashMap<String,String>();
         map.put("id","1");
         map.put("name","tl");
         map.put("gren","男");
        Jedis jedis=new Jedis("127.0.0.1",6379);
//        jedis.hset("student","name","tuliang");
//        jedis.hset("student","id","1");
//        jedis.hset("student","gren","男");
         jedis.hmset("student1",map);
        List<String> hmget = jedis.hmget("student1","id","name","gren");
             hmget.forEach(System.out::println);
    }
}
