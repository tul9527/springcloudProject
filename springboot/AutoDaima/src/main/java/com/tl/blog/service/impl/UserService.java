package com.tl.blog.service.impl;

import com.tl.blog.entity.User;
import com.tl.blog.mapper.UserMapper;
import com.tl.blog.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 涂亮
 * @since 2022-03-11
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> implements IUserService {

}
