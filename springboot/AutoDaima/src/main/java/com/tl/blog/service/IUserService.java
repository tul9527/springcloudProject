package com.tl.blog.service;

import com.tl.blog.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 涂亮
 * @since 2022-03-11
 */
public interface IUserService extends IService<User> {

}
