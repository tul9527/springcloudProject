package com.tl.blog.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 涂亮
 * @since 2022-03-11
 */
@RestController
@RequestMapping("/blog/user")
public class UserController {

}

