package com.tl.blog.mapper;

import com.tl.blog.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 涂亮
 * @since 2022-03-11
 */
public interface UserMapper extends BaseMapper<User> {

}
