package com.tl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoDaimaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoDaimaApplication.class, args);
    }

}
