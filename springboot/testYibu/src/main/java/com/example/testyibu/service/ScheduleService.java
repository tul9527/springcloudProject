package com.example.testyibu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class ScheduleService {
    @Autowired
    JavaMailSenderImpl mailSender;
    //在一个特定的时间执行这个方法
    // 秒 分  时  日 月 周几
    int i=0;
    @Scheduled(cron="0 */1 * * * ?")
    public void hello() throws MessagingException {

        MimeMessage mailMessage=mailSender.createMimeMessage();
        MimeMessageHelper helper=new MimeMessageHelper(mailMessage,true);
        helper.setSubject("涂亮你好啊plus"+i++);
        if(i<5){
        helper.setText("<p style='color: red'>邮件发送，敬请收</p>",true);
        //附件
        helper.addAttachment(i+".jpeg",new File("C:\\Users\\lenovo\\Desktop\\"+i+".jpeg"));
        helper.setTo("li_xiang_0731@qq.com");
        helper.setFrom("405208660@qq.com");
        mailSender.send(mailMessage);
        }
    }
}
