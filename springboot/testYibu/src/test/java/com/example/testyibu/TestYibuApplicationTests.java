package com.example.testyibu;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Arrays;

@SpringBootTest
class TestYibuApplicationTests {

    @Autowired
    JavaMailSenderImpl mailSender;

    //简单的文件发送
//    @Test
//    void contextLoads() {
//        SimpleMailMessage mailMessage=new SimpleMailMessage();
//        mailMessage.setSubject("涂亮你好啊");
//        mailMessage.setText("邮件发送，敬请收");
//
//        mailMessage.setTo("li_xiang_0731@qq.com");
//        mailMessage.setFrom("405208660@qq.com");
//        mailSender.send(mailMessage);
//    }
   //复杂的邮件发送，包括图片文件（附件）


    @Test
    void contextLoads1() throws MessagingException {
        MimeMessage mailMessage=mailSender.createMimeMessage();
        MimeMessageHelper helper=new MimeMessageHelper(mailMessage,true);
        helper.setSubject("涂亮你好啊plus");
        helper.setText("<p style='color: red'>邮件发送，敬请收</p>",true);
        //附件
        helper.addAttachment("1.jpg",new File("C:\\Users\\lenovo\\Desktop\\1.jpg"));
        helper.setTo("li_xiang_0731@qq.com");
        helper.setFrom("405208660@qq.com");
        mailSender.send(mailMessage);
    }

}
