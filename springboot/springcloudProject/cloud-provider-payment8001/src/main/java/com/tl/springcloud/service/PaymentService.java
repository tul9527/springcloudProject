package com.tl.springcloud.service;

import com.tl.springcloud.pojo.Payment;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @program: springcloudProject
 * @description: PaymentService接口
 * @author: Mr.Tu
 * @create: 2022-04-06 18:22
 **/
public interface PaymentService {
    public  int add(Payment payment);

    public  Payment getPaymentById(@Param("id") Long id);

    List<Payment> getAll();
}
