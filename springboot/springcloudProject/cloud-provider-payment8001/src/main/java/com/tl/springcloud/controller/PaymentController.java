package com.tl.springcloud.controller;

import com.tl.springcloud.pojo.CommentResult;
import com.tl.springcloud.pojo.Payment;
import com.tl.springcloud.service.PaymentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: springcloudProject
 * @description: controll
 * @author: Mr.Tu
 * @create: 2022-04-06 21:44
 **/
@RestController
public class PaymentController {
@Resource
    PaymentService service;
@PostMapping("/payment/add")
    public CommentResult add(Payment payment){
     int result=service.add(payment);

     if(result>0){
         return  new CommentResult(200,"插入成功",result);
     }else {
         return  new CommentResult(444,"插入失败",null);
     }
    }
  @GetMapping("/payment/get/{id}")
    public CommentResult getById(@PathVariable Long id){
    Payment payment=service.getPaymentById(id);
    if(payment!=null){
        return new CommentResult(200,"查询成功",payment);
    }else {
        return  new CommentResult(444,"查询失败",null);
    }

  }
}
