package com.tl.springcloud.service.impl;

import com.tl.springcloud.dao.PaymentDao;
import com.tl.springcloud.pojo.Payment;
import com.tl.springcloud.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: springcloudProject
 * @description: PaymentService实现类
 * @author: Mr.Tu
 * @create: 2022-04-06 18:29
 **/
@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    PaymentDao paymentDao;
    @Override
    public int add(Payment payment) {
     return paymentDao.add(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {

        return paymentDao.getPaymentById(id);
    }

    @Override
    public List<Payment> getAll() {
        return paymentDao.getAll();
    }
}
