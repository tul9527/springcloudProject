package com.tl.springcloud.dao;

import com.tl.springcloud.pojo.Payment;
import org.apache.ibatis.annotations.Mapper;



import java.util.List;

@Mapper
public interface PaymentDao {
      int add(Payment payment);
      Payment getPaymentById(Long id);
      List<Payment>  getAll();
}
