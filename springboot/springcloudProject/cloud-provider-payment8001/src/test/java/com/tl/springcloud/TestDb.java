package com.tl.springcloud;

import com.tl.springcloud.dao.PaymentDao;

import com.tl.springcloud.pojo.Payment;
import com.tl.springcloud.service.PaymentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


/**
 * @program: springcloudProject
 * @description: 测试类
 * @author: Mr.Tu
 * @create: 2022-04-06 19:54
 **/
@SpringBootTest(classes = {PaymentService.class})
@RunWith(SpringRunner.class)
public class TestDb {
  @Autowired
   private PaymentService paymentService;

  @Autowired
  PaymentDao paymentDao;
  @Test
    public void testDb(){
    System.out.println("ssss");
   // paymentService.add(new Payment(3l,"dd"));
    List<Payment>  list=paymentDao.getAll();

    list.forEach(System.out::println);
      //System.out.println(paymentService.getAll());
  }
}
