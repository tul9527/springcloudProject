package com.tl.springbootshiro.mapper;


import com.tl.springbootshiro.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

//这个注解表示了这是个mybatis的mapper类
@Mapper
@Repository
public interface UserMapper {
    List<User> queryUserList();
    User queryUserById(int id);
    User queryUserByName(String name);
    int addUser(User user);
    int updateUser(User user);
    int deleteUser(User user);
}
