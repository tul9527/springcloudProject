package com.tl.springbootshiro.config;


import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {
    //shiroFileterFactoryBeam
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean bean=new ShiroFilterFactoryBean();
        System.out.println("2");
       //设置安全管理器
        bean.setSecurityManager(defaultWebSecurityManager);
        //添加shiro的内置过滤器
        /*
        *   anon: 无需验证就可以访问
        *   authc: 必须验证了才能访问
        *  user ： 必须拥有 记住我 功能才能用
        *  perms： 拥有对某个资源的权限才能访问
        *  role：  拥有某个角色权限才能访问
        * */
        Map<String,String>  filterMap=new LinkedHashMap<>();
        //授权，正常的情况下，没有授权会跳转到未授权页面
        filterMap.put("/user/add","perms[user:add]");
        filterMap.put("/user/update","perms[user:update]");
       // filterMap.put("/user/*","authc");

        bean.setFilterChainDefinitionMap(filterMap);
        bean.setLoginUrl("/tologin");
        bean.setUnauthorizedUrl("/unauth");
        return bean;
    }
    // DefaultWebSecurityManger
    @Bean(name="securityManager")
   public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm){
        System.out.println("1");
        DefaultWebSecurityManager securityManager=new DefaultWebSecurityManager();
       securityManager.setRealm(userRealm);
        return securityManager;
   }

    //创建realm 对象，需要自定义类
    @Bean
   public UserRealm userRealm(){
      return new UserRealm();
  }

  @Bean
  public ShiroDialect getShiroDialect(){
        return new ShiroDialect();
  }

}
