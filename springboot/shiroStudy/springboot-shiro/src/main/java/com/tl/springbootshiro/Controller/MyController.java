package com.tl.springbootshiro.Controller;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.jws.WebParam;

@Controller
public class MyController {

    @RequestMapping({"/","index"})
    public  String  toIndex(Model model){
        model.addAttribute("msg","hello,shiro");
        return "index";

    }

    @RequestMapping("/user/add")
    public  String add(){
        return "user/add";
    }
    @RequestMapping("/user/update")
    public String  update(){
        return  "user/updata";
    }

    @RequestMapping("/tologin")
    public String toLogin(){
        return "user/login";
    }


  @RequestMapping("/login")
     public String login (String username,String password,Model model){
         Subject subject= SecurityUtils.getSubject();
         UsernamePasswordToken token=new UsernamePasswordToken(username,password);
        // token.setRememberMe(true);
         try {
             System.out.println("进入断点");
             subject.login(token);
             return "index";
         }catch (UnknownAccountException e){
             model.addAttribute("msg","用户不存在");
             return "user/login";
         }catch (IncorrectCredentialsException e){
             model.addAttribute("msg","密码错误");
             return "user/login";
         }

     }
     @RequestMapping("/unauth")
     @ResponseBody
     public String unauthorized(){
        return "未授权，无法访问此页面";
     }
}
