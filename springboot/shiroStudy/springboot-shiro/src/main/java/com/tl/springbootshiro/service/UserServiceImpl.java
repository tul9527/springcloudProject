package com.tl.springbootshiro.service;

import com.tl.springbootshiro.mapper.UserMapper;
import com.tl.springbootshiro.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl  implements UserService{
    @Autowired
    UserMapper userMapper;
    @Override
    public List<User> queryUserList() {
        return userMapper.queryUserList();
    }
    @Override
    public User queryUserById(int id) {
        return userMapper.queryUserById(id);
    }
    @Override
    public User queryUserByName(String name) {
        return userMapper.queryUserByName(name);
    }
    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }
    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public int deleteUser(User user) {
        return 0;
    }
}
