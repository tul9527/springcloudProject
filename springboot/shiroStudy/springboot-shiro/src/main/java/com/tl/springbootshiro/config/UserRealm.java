package com.tl.springbootshiro.config;


import com.tl.springbootshiro.pojo.User;
import com.tl.springbootshiro.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

//自定义的Realm
public class UserRealm  extends AuthorizingRealm {
    @Autowired
    UserService userService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("执行了授权");
        SimpleAuthorizationInfo info=new SimpleAuthorizationInfo();
        //授权
        //获得登录的对象，对象由验证SimpleAuthenticationInfo（）方法传
        Subject subject=SecurityUtils.getSubject();
         User user=(User) subject.getPrincipal();
         if(user.getQuanxian().equals("user:add")){
             info.addStringPermission("user:add");
         }if(user.getQuanxian().equals("user:update")){
            info.addStringPermission("user:update");
        }
        return info;
    }
   //验证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        System.out.println("执行了验证");
        //获取当前的用户
      //  Subject subject= SecurityUtils.getSubject();
//        String name="root";
//        String password="123456";
        UsernamePasswordToken userToken=(UsernamePasswordToken) token;
        User user=userService.queryUserByName(userToken.getUsername());
           if(user==null){
              return null;
           }
           Subject currentSubject=SecurityUtils.getSubject();
           Session session=currentSubject.getSession();
           session.setAttribute("loginUser",user);
           //可以加密，MD5  MD5盐值加密
          //密码认证  shiro 加密了
        return new SimpleAuthenticationInfo(user,user.getPwd(),"");
    }
}
