package com.tl.springbootshiro;

import com.tl.springbootshiro.pojo.User;
import com.tl.springbootshiro.service.UserService;
import com.tl.springbootshiro.service.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

@SpringBootTest
class SpringbootShiroApplicationTests {
     @Autowired
     UserService userService;
    @Test
    void contextLoads() {
      User user=userService.queryUserByName("tl");
        System.out.println(user);
    }

}
