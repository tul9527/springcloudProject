package com.tl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tl.mapper.UserMapper;
import com.tl.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WapperTest {
    @Autowired
    private UserMapper userMapper;
    @Test
    public void contextLoads() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.isNotNull("email")
                .isNotNull("name")
                .ge("age",12); //大于等于12
        System.out.println(userMapper.selectCount(wrapper));
        userMapper.selectList(wrapper).forEach(System.out::println);

    }
    @Test
    public void betweenAndTest() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.between("age",4,12);
        userMapper.selectList(wrapper).forEach(System.out::println);

    }
    @Test
    public void likeTest() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.notLike("name","c")
               .likeRight("name","t");

        //userMapper.selectList(wrapper).forEach(System.out::println);
         userMapper.selectMaps(wrapper).forEach(System.out::println);
    }
   //关联查询
    @Test
    public void guanlianTest() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.inSql("id","select id from user where id>5");
        userMapper.selectList(wrapper).forEach(System.out::println);
        userMapper.selectObjs(wrapper).forEach(System.out::println);
    }
}
