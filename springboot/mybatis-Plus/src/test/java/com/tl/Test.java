package com.tl;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


//lambda表达式测试
public class Test {
//    public static void main(String[] args) {
//        List list=new ArrayList();
//        list.add("sss");
//        list.add("dddd");
//        list.forEach(item ->{
//            if(item.equals("sss")){
//                System.out.println("list中有sss,这是判断语句");
//            }
//            System.out.println("item:"+item);
//        });
//        list.removeIf(item-> item.equals("dddd"));
//        list.forEach(System.out::println);
//    }

//    public static void main(String[] args) {
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i <100 ; i++) {
//                    System.out.println("i="+i);
//                }
//            }
//        }) ;
//
//
//          Thread thread1=new Thread(()->{
//              for (int i = 0; i < 100; i++) {
//                  System.out.println("m="+i);
//              }
//          });
//              thread.start();
//              thread1.start();
//        }

    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
       // numbers.forEach(item-> System.out.println(item++));
//        List<Integer> mapped = numbers.stream().map(x -> x * 2).collect(Collectors.toList());
//        mapped.forEach(System.out::println);
       // numbers.forEach(System.out::println);
        numbers.stream().map(x->x*2).collect(Collectors.toList()).forEach(System.out::println);

    }
    }