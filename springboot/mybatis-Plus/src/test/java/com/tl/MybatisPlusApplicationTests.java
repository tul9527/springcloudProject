package com.tl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tl.mapper.UserMapper;
import com.tl.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MybatisPlusApplicationTests {
    @Autowired
   private UserMapper userMapper;
    @Test
   public void contextLoads() {
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
//        for (User user: users) {
//            System.out.println(user.toString());
//        }
    }
    @Test
    public void TestInsert(){
        User user = new User();
        user.setName("涂亮");
        user.setAge(5);
        user.setEmail("456516@qq.com");
        int insert = userMapper.insert(user);
        System.out.println(insert);
    }
    @Test
    public void TestUpdate(){
        User user = new User();
        user.setName("涂亮cccc");
        user.setId(6l);
        userMapper.updateById(user);
    }
    //测试乐观锁成功
    @Test
    public void testOptimisticLocker(){
        User user = userMapper.selectById(1l);
        user.setName("lx");
        user.setEmail("sdfsdf@45");

        User user1 = userMapper.selectById(1l);
        user1.setName("lx111");
        user1.setEmail("sdfsdf@4511111");
        userMapper.updateById(user1);

        userMapper.updateById(user);
    }
   //通过id查询多条数据
    @Test
    public void testSelect(){
        List<User> users = userMapper.selectBatchIds(Arrays.asList(1l, 2l, 3l));
        users.forEach(System.out::println);
    }
    //条件查询每个map的put在sql 中是and
    @Test
    public void testMapSelect(){
        HashMap<String,Object> map=new HashMap<String,Object>();
        //map.put("name","lx111");
        map.put("age",5);
        List<User> users = userMapper.selectByMap(map);
        users.forEach(System.out::println);
    }
    @Test
    public void testPage(){
        //第一个参数第几页，数据条数
        Page<User> page=new Page<>(4,5);
        IPage<User> userIPage = userMapper.selectPage(page, null);
        userIPage.getRecords().forEach(System.out::println);
    }

    @Test
    public void testDelete(){
        userMapper.deleteById(2l);
    }


}
